#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include "ListaDinEncad.h"
#include "duplamente.h"
/*
    remova o comentario de qual lista quer testar
    ListaDinEncad = lista dimanima encadiada
    duplamente = lista duplamente encadiada com decritor
*/
int main(int argc, char const *argv[])
{
    Lista *li;
    int matricula;
    int mat;
    int pos;
    float n1,n2,n3;

    li = cria_lista();

    int oqFazer = 1;

    while (oqFazer){

    printf("O quer fazer? 0 = sair 1 = adiciona aluno no inicio\n"
    "2 = adiciona aluno no final 3 = adiciona aluno de forma ordenada\n"
    "4 = remove aluno no inicio 5 = remove aluno no final\n"
    "6 = remove aluno por matricula 7 = busca pela posição\n"
    "8 = busca pelo valor 9 = mostra lista completa\n");

    scanf("%d", &oqFazer);

    switch(oqFazer)
    {
    case 1:
        printf("digite os dados do aluno:\n ");

        printf("matricula: ");
        scanf("%d", &matricula);

        printf("nota 1: ");
        scanf( "%f", &n1);

        printf("nota 2: ");
        scanf("%f", &n2);

        printf("nota 3: ");
        scanf( "%f", &n3);

        struct aluno ale = {matricula, n1, n2, n3};

        if(insere_lista_inicio(li, ale))
            printf("Aluno inserido com sucesso\n");

        break;
    case 2:
        printf("digite os dados do aluno:\n ");

        printf("matricula: ");
        scanf("%d", &matricula);

        printf("nota 1: ");
        scanf( "%f", &n1);

        printf("nota 2: ");
        scanf( "%f", &n2);

        printf("nota 3: ");
        scanf( "%f", &n3);

        struct aluno alo = {matricula, n1, n2, n3};

        if(insere_lista_final(li, alo))
            printf("Aluno inserido com sucesso\n");

        break;
    case 3:
        printf("digite os dados do aluno:\n ");

        printf("matricula: ");
        scanf("%d", &matricula);

        printf("nota 1: ");
        scanf( "%f", &n1);

        printf("nota 2: ");
        scanf( "%f", &n2);

        printf("nota 3: ");
        scanf( "%f", &n3);

        struct aluno ali = {matricula, n1, n2, n3};

        if(insere_lista_ordenada(li, ali))
            printf("Aluno inserido com sucesso\n");
        break;

    case 4:
        if(remove_lista_inicio(li))
            printf("aluno removido com sucesso\n");
        break;

    case 5:
        if(remove_lista_final(li))
            printf("aluno removido com sucesso\n");
        break;

    case 6:

        printf("digite a matricula do meliante ");
        scanf("%d", &mat);

        if(remove_lista_qualquer(li, mat))
            printf("aluno removido com sucesso\n");
        break;

    case 7:
        printf("Digite a posicao");
        scanf("%d", &pos);

        struct aluno ala;

        if(consulta_lista_pos(li, pos, &ala))
            printf("As notas do aluno são:n1 = %.2f, n2 = %.2f n3 = %.2f\nEnter para continuar", ala.n1, ala.n2, ala.n3);
            scanf("");
        break;
    case 8:
        printf("Digite a posicao");
        scanf("%d", &mat);

        struct aluno alu;

        if(consulta_lista_valor(li, mat, &alu))
            printf("As notas do aluno são:n1 = %.2f, n2 = %.2f n3 = %.2f\nEnter para continuar", alu.n1, alu.n2, alu.n3);
            scanf("");
        break;

    case 9:
        mostra_toda(li);
        printf("nEnter para continuar");
        scanf("");
    default:
        break;

    libera_lista(li);
    return 0;
}
}
}
