struct aluno
{
    int matricula;
    float n1;
    float n2;
    float n3;
};
typedef struct elemento* Lista;

Lista* cria_lista();
void libera_lista(Lista* li);

//informações da lista
int tamanho_lista(Lista* li);
int lista_vazia(Lista* li);

//inserindo na lista
int insere_lista_inicio(Lista* li, struct aluno al);
int insere_lista_final(Lista* li, struct aluno al);
int insere_lista_ordenada(Lista* li, struct aluno al);

//removendo da lista
int remove_lista_inicio(Lista* li);
int remove_lista_final(Lista* li);
int remove_lista_qualquer(Lista* li, int mat);

//consultando lista
int consulta_lista_pos(Lista* li, int pos, struct aluno *al);
int consulta_lista_valor(Lista* li, int valor, struct aluno *al);
void mostra_toda(Lista* li);
