#include <stdio.h>
#include <stdlib.h>

#include "duplamente.h"

struct elemento{
  struct elemento *ant;
  struct aluno dados;
  struct elemento *prox;
};
typedef struct elemento Elem;
struct descritor{
  struct elemento *inicio;
  struct elemento *final;
  int qtd;
};

Lista* cria_lista(){
  Lista* li = (Lista*) malloc(sizeof(Lista));
  if(li != NULL){
    li->inicio = NULL;
    li->final = NULL;
    li->qtd = 0;

  }
  return li;
}
void libera_lista(Lista* li){
  if(li != NULL){
    Elem* no;
    while((li->inicio) != NULL){
      no = li->inicio;
      li->inicio = li->inicio->prox;
      free(no);
    }
    free(li);
  }
}
int tamanho_lista(Lista*li){
  if(li == NULL)
    return 0;
  return li->qtd;
}
int lista_vazia(Lista* li){
  if(li == NULL) return 1;
  //if(*li == NULL) return 1;

  return 0;
}
int insere_lista_inicio(Lista* li, struct aluno al){
  if(li == NULL) return 0;
  Elem* no = (Elem*) malloc(sizeof(Elem));
  if(no == NULL) return 0;
  no->dados = al;
  no->prox = li->inicio;
  no->ant = NULL;
  //lista não vazia: apontar para o anterior!
  if(li->inicio == NULL)
    li->final = no;
  li->inicio = no;
  li->qtd++;
  return 1;
}

int insere_lista_final(Lista* li, struct aluno al){
  if(li == NULL) return 0;
  Elem* no = (Elem*) malloc(sizeof(Elem));
  if(no == NULL) return 0;
  no->dados = al;
  no->prox = NULL;
  if(li->inicio == NULL){//lista vazia
    no->ant = NULL;
    li->inicio = no;
  }else{
    no->ant = li->final;
  }

  li->final = no;
  li->qtd++;
  return 1;
}
int insere_lista_ordenada(Lista* li, struct aluno al){
  if(li == NULL) return 0;
  Elem* no = (Elem*) malloc(sizeof(Elem));
  if(no == NULL) return 0;
  no->dados = al;
  if(lista_vazia(li)){//insere no inicio
    no->prox = NULL;
    no->ant = NULL;
    li->inicio = no;
    li->qtd++;
    return 1;
  }else{//procura onde inserir
    Elem *ante, *atual = li->inicio;
    while(atual != NULL && atual->dados.matricula < al.matricula){
      ante = atual;
      atual = atual->prox;
    }
    if(atual == li->inicio){//insere no inicio
      no->ant = NULL;
      li->inicio->ant = no;
      no->prox = li->inicio;
      li->inicio = no;
    }else{
      no->prox = ante->prox;
      no->ant = ante;
      ante->prox = no;
      if(atual != NULL)
        atual->ant = no;
    }
    li->qtd++;
    return 1;
  }
}
int remove_lista_inicio(Lista* li){
  if(li == NULL) return 0;
  if(li->inicio == NULL) return 0;

  Elem *no = li->inicio;
  li->inicio = no->prox;
  if(no->prox != NULL)
    no->prox->ant = NULL;
  if(li->inicio == NULL)
    li->final = NULL;
  li->qtd--;
  free(no);
  return 1;
}
int remove_lista_final(Lista* li){
  if(li == NULL) return 0;
  if(li->inicio == NULL) return 0;

  Elem *no = li->final;
  // while(no->prox != NULL)
  //   no = no->prox;

  if(no->ant == NULL)
    li->inicio = no->prox;
  else
    no->ant->prox = NULL;

  li->qtd--;
  free(no);
  return 1;
}
int remove_lista_qualquer(Lista *li, int mat){
  if(li == NULL) return 0;
  Elem *no = li->inicio;
  while(no != NULL && no->dados.matricula != mat){
    no = no->prox;
  }
  if(no == NULL) return 0;//nao encontrado

  if(no->ant == NULL)//remove o primero
    li->inicio = no->prox;
  else//remove no meio
    no->ant->prox = no->prox;

  if(no->prox != NULL)//caso nao seja o ultimo
    no->prox->ant = no->ant;
  free(no);
  li->qtd--;
  return 1;
}
int consulta_lista_pos(Lista* li, int pos, struct aluno *al){
  if(li == NULL || pos <= 0) return 0;
  Elem *no = li->inicio;
  int i = 1;
  while(no != NULL && i < pos){
    no = no->prox;
    i++;
  }
  if(no == NULL)
   return 0;
  else{
    *al = no->dados;
    return 1;
  }
}
int consulta_lista_valor(Lista* li, int mat, struct aluno *al){
  if(li == NULL) return 0;
  Elem *no = li->inicio;
  while (no != NULL && no->dados.matricula != mat) {
    no = no->prox;
  }
  if(no == NULL)
    return 0;
  else{
    *al = no->dados;
    return 1;
  }
}
void mostra_toda(Lista* li){

    Elem* no = li->inicio;
    while(no != NULL){
        printf("As notas do aluno da matricula %d são:n1 = %.2f, n2 = %.2f n3 = %.2f\n", no->dados.matricula, no->dados.n1, no->dados.n2, no->dados.n3);
        no = no->prox;
    }
}
